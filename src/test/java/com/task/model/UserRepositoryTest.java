package com.task.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.task.repository.UserRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

  @Autowired
  private UserRepository repository;

  @Test
  public void checkCount() {
    long count = this.repository.count();
    assertEquals(4, count);
  }

  @Test
  public void checkUserWithLoan() {
    User userWithTwoLoans = this.repository.findOne(1L);
    assertNotNull(userWithTwoLoans);
    assertEquals(2, userWithTwoLoans.getLoans().size());
  }
}