package com.task.model;

import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.task.repository.LoanRepository;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LoanRepositoryTest {

  private static final long ONE_WEEK = 604800000L;
  private static final long ONE_MINUTE = 60000L;
  private static final String COUNTRY_CODE = "LV";

  @Autowired
  private LoanRepository repository;


  @Test
  public void checkLoansCount() {
    long count = this.repository.count();
    assertEquals(count, 3);
  }

  @Test
  public void findLoansByUserId() {
    List<Loan> userWithTwoLoans = this.repository.findByUserId(1L);
    assertEquals(2, userWithTwoLoans.size());

    List<Loan> userWithNoLoans = this.repository.findByUserId(2L);
    assertEquals(0, userWithNoLoans.size());

    List<Loan> userWithOneLoan = this.repository.findByUserId(3L);
    assertEquals(1, userWithOneLoan.size());
  }

  @Test
  public void findByCreatedBetweenAndCountryCode_InOneMinute() {
    Date startDate = new Date(System.currentTimeMillis() - ONE_MINUTE);
    Date endDate = new Date();
    List<Loan> loans = this.repository.findByCreatedBetweenAndCountryCode(startDate, endDate, COUNTRY_CODE);
    assertEquals(1, loans.size());
  }

  @Ignore
  @Test
  public void findByCreatedBetweenAndCountryCode_InOneWeek() {
    Date startDate = new Date(System.currentTimeMillis() - ONE_WEEK);
    Date endDate = new Date();
    List<Loan> loans = this.repository.findByCreatedBetweenAndCountryCode(startDate, endDate, COUNTRY_CODE);
    assertEquals(3, loans.size());
  }
}