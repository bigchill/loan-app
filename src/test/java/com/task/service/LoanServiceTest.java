package com.task.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.task.config.LoanAppProperties;
import com.task.controller.api.LoanRequest;
import com.task.error.BlacklistUser;
import com.task.error.LoanNotFound;
import com.task.error.LoanRequestLimitExceeded;
import com.task.intergration.dto.CountryResponse;
import com.task.intergration.exception.CountryNotFound;
import com.task.intergration.service.IpResolverService;
import com.task.model.Loan;
import com.task.repository.LoanRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoanServiceTest {

  private static final String REMOTE_ADDRESS = "62.63.137.2";

  private LoanService service;

  @Mock private LoanRepository loanRepositoryMock;
  @Mock private BlacklistService blacklistServiceMock;
  @Mock private IpResolverService ipResolverServiceMock;
  @Mock private LoanAppProperties loanAppPropertiesMock;

  @Before
  public void setUp() {
    service = new LoanService(loanRepositoryMock, blacklistServiceMock, ipResolverServiceMock, loanAppPropertiesMock);
  }


  @Test
  public void findAllLoans_Ok() {
    when(loanRepositoryMock.findAll()).thenReturn(createLoans());
    List<Loan> loans = service.findAllLoans();
    assertEquals(2, loans.size());
  }


  @Test
  public void findUserLoans_Ok() {
    when(loanRepositoryMock.findByUserId(anyLong())).thenReturn(createLoans());
    List<Loan> userLoans = service.findUserLoans(anyLong());
    assertEquals(2, userLoans.size());
  }

  @Test(expected = LoanNotFound.class)
  public void findUserLoans_NotFound() {
    when(loanRepositoryMock.findByUserId(anyLong())).thenReturn(null);
    service.findUserLoans(anyLong());
  }


  @Test(expected = BlacklistUser.class)
  public void applyForLoan_user_from_Blacklist() {
    when(blacklistServiceMock.isBlacklistUser(anyString())).thenReturn(true);
    service.applyForLoan(new LoanRequest(), REMOTE_ADDRESS);
  }

  @Test
  public void applyForLoan_ip_resolver_failed_default_country_used() {
    when(blacklistServiceMock.isBlacklistUser("A45366D2-B4F7")).thenReturn(false);
    when(ipResolverServiceMock.resolveCountry(anyString())).thenThrow(CountryNotFound.class);
    when(loanAppPropertiesMock.getLoanRequestCountLimit()).thenReturn(10);
    when(loanAppPropertiesMock.getLoanRequestIntervalLimitInSeconds()).thenReturn(2000);
    when(loanRepositoryMock.findByCreatedBetweenAndCountryCode(any(), any(), anyString())).thenReturn(createLoans());
    when(loanRepositoryMock.save(any(Loan.class))).thenReturn(createLoan());

    Loan loan = service.applyForLoan(createLoanRequest(), anyString());

    assertNotNull(loan);
    assertEquals("LV", loan.getCountryCode());
    assertEquals(new BigDecimal(2500), loan.getAmount());
  }


  @Test(expected = LoanRequestLimitExceeded.class)
  public void applyForLoan_too_many_requests() {
    when(blacklistServiceMock.isBlacklistUser("A45366D2-B4F7")).thenReturn(false);
    when(ipResolverServiceMock.resolveCountry(REMOTE_ADDRESS)).thenReturn(new CountryResponse("LV", "Latvia"));
    when(loanAppPropertiesMock.getLoanRequestCountLimit()).thenReturn(2);
    when(loanAppPropertiesMock.getLoanRequestIntervalLimitInSeconds()).thenReturn(2000);
    when(loanRepositoryMock.findByCreatedBetweenAndCountryCode(any(), any(), anyString())).thenReturn(createLoans());
    service.applyForLoan(createLoanRequest(), REMOTE_ADDRESS);
  }

  @Test
  public void applyForLoan_Ok() {
    when(blacklistServiceMock.isBlacklistUser("A45366D2-B4F7")).thenReturn(false);
    when(ipResolverServiceMock.resolveCountry(REMOTE_ADDRESS)).thenReturn(new CountryResponse("LV", "Latvia"));
    when(loanAppPropertiesMock.getLoanRequestCountLimit()).thenReturn(10);
    when(loanAppPropertiesMock.getLoanRequestIntervalLimitInSeconds()).thenReturn(2000);
    when(loanRepositoryMock.findByCreatedBetweenAndCountryCode(any(), any(), anyString())).thenReturn(createLoans());
    when(loanRepositoryMock.save(any(Loan.class))).thenReturn(createLoan());

    Loan res = service.applyForLoan(createLoanRequest(), REMOTE_ADDRESS);

    assertNotNull(res);
    assertEquals("LV", res.getCountryCode());
  }


  private LoanRequest createLoanRequest() {
    LoanRequest loanRequest = new LoanRequest();
    loanRequest.amount = new BigDecimal(2500);
    loanRequest.currency = "EUR";
    loanRequest.interestRate = new BigDecimal(12.5);
    loanRequest.term = 180;
    loanRequest.personalId = "E324A-12349201";
    loanRequest.userId = 1L;
    return loanRequest;
  }

  private List<Loan> createLoans() {
    List<Loan> loans = new ArrayList<>();
    loans.add(new Loan());
    loans.add(new Loan());
    return loans;
  }

  private Loan createLoan() {
    Loan loan = new Loan();
    loan.setAmount(new BigDecimal(2500));
    loan.setInterestRate(new BigDecimal(12.5));
    loan.setTerm(180);
    loan.setCountryCode("LV");
    loan.setUserId(1L);
    return loan;
  }
}