package com.task.service;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.task.model.Blacklist;
import com.task.repository.BlacklistRepository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BlacklistServiceTest {

  private BlacklistService service;

  @Mock
  private BlacklistRepository blacklistRepositoryMock;

  @Before
  public void setUp() {
    service = new BlacklistService(blacklistRepositoryMock);
  }

  @Test
  public void isBlacklistUser_blacklisted() {
    when(blacklistRepositoryMock.findByPersonalId(anyString())).thenReturn(Optional.of(new Blacklist()));
    boolean isBlacklistUser = service.isBlacklistUser("E324A-12349201");
    assertTrue(isBlacklistUser);
  }

  @Test
  public void isBlacklistUser_notBlacklisted() {
    when(blacklistRepositoryMock.findByPersonalId(anyString())).thenReturn(Optional.empty());
    boolean isBlacklistUser = service.isBlacklistUser("NOT-FOUND-12349201");
    assertFalse(isBlacklistUser);
  }
}