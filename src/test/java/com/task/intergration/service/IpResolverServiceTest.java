package com.task.intergration.service;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.task.config.LoanAppProperties;
import com.task.intergration.dto.CountryResponse;
import com.task.intergration.exception.CountryNotFound;
import com.task.intergration.exception.IpResolverServiceUnavailable;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IpResolverServiceTest {

  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private LoanAppProperties ipResolverProperties;

  @Autowired
  private IpResolverService service;

  private MockRestServiceServer mockServer;

  @Rule
  public ExpectedException expectedExc = ExpectedException.none();

  @Before
  public void setUp() {
    mockServer = MockRestServiceServer.createServer(restTemplate);
    service = new IpResolverService(restTemplate, ipResolverProperties);
  }


  @Test
  public void resolveCountry_Ok() {
    mockServer.expect(requestTo("http://freegeoip.net/json/12.23.34.45"))
        .andExpect(method(HttpMethod.GET))
        .andRespond(withSuccess(returnCountry(), MediaType.APPLICATION_JSON));
    CountryResponse countryResponse = service.resolveCountry("12.23.34.45");
    mockServer.verify();

    assertThat(countryResponse.getCountryName()).isEqualTo("United States");
    assertThat(countryResponse.getCountryCode()).isEqualTo("US");
  }


  @Test
  public void resolveCountry_NotFound() {
    expectedExc.expect(CountryNotFound.class);
    expectedExc.expectMessage("Not found country for IP address: 12.23.34.45");

    mockServer.expect(requestTo("http://freegeoip.net/json/12.23.34.45"))
        .andExpect(method(HttpMethod.GET))
        .andRespond(withStatus(HttpStatus.NOT_FOUND));
    service.resolveCountry("12.23.34.45");
    mockServer.verify();
  }


  @Test
  public void resolveCountry_InternalServerError() {
    expectedExc.expect(IpResolverServiceUnavailable.class);
    expectedExc.expectMessage("IP Resolver service is currently unavailable");

    mockServer.expect(requestTo("http://freegeoip.net/json/12.23.34.45"))
        .andExpect(method(HttpMethod.GET))
        .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR));
    service.resolveCountry("12.23.34.45");
    mockServer.verify();
  }


  @Test
  public void resolveCountry_AnyOtherError() {
    expectedExc.expect(IpResolverServiceUnavailable.class);
    expectedExc.expectMessage("Something wrong with IP Resolver service");

    mockServer.expect(requestTo("http://freegeoip.net/json/12.23.34.45"))
        .andExpect(method(HttpMethod.GET))
        .andRespond(withStatus(HttpStatus.TOO_MANY_REQUESTS));
    service.resolveCountry("12.23.34.45");
    mockServer.verify();
  }

  private String returnCountry() {
    return  "{\n" +
        "  \"ip\": \"12.23.34.45\",\n" +
        "  \"country_code\": \"US\",\n" +
        "  \"country_name\": \"United States\",\n" +
        "  \"region_code\": \"\",\n" +
        "  \"region_name\": \"\",\n" +
        "  \"city\": \"\",\n" +
        "  \"zip_code\": \"\",\n" +
        "  \"time_zone\": \"\",\n" +
        "  \"latitude\": 37.751,\n" +
        "  \"longitude\": -97.822,\n" +
        "  \"metro_code\": 0\n" +
        "}";
  }

}