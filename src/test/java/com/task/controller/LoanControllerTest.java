package com.task.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.task.controller.api.LoanRequest;
import com.task.error.BlacklistUser;
import com.task.error.LoanNotFound;
import com.task.error.LoanRequestLimitExceeded;
import com.task.model.Loan;
import com.task.service.LoanService;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(LoanController.class)
public class LoanControllerTest {

  private static final Long USER_WITHOUT_LOANS = 4L;
  private static final Long USER_WITH_LOANS = 1L;

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  LoanService loanServiceMock;

  private JacksonTester<LoanRequest> jsonTester;

  private LoanRequest loanRequest;

  @Before
  public void setUp() {
    ObjectMapper objectMapper = new ObjectMapper();
    JacksonTester.initFields(this, objectMapper);

    loanRequest = new LoanRequest();
  }

  @Test
  public void getByUserId_ok() throws Exception {
    given(this.loanServiceMock.findUserLoans(USER_WITH_LOANS)).willReturn(createdLoanList());

    this.mockMvc.perform(get("/loans/" + USER_WITH_LOANS).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(2)));
  }

  @Test
  public void getByUserId_notFound() throws Exception {
    given(this.loanServiceMock.findUserLoans(USER_WITHOUT_LOANS)).willThrow(LoanNotFound.class);

    this.mockMvc.perform(get("/loans/" + USER_WITHOUT_LOANS).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }


  @Test
  public void list_ok() throws Exception {
    given(this.loanServiceMock.findAllLoans()).willReturn(createdLoanList());

    this.mockMvc.perform(get("/loans/").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(2)));
  }

  @Test
  public void list_empty() throws Exception {
    given(this.loanServiceMock.findAllLoans()).willReturn(Collections.emptyList());

    this.mockMvc.perform(get("/loans/").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(0)));
  }


  @Test
  public void apply_ok() throws Exception {
    String loanRequestJson = jsonTester.write(loanRequest).getJson();
    given(this.loanServiceMock.applyForLoan(any(LoanRequest.class), anyString())).willReturn(new Loan());

    this.mockMvc.perform(post("/loans/apply").contentType(MediaType.APPLICATION_JSON)
        .content(loanRequestJson))
        .andExpect(status().isOk());
  }


  @Test
  public void apply_limit_per_country_exceeded() throws Exception {
    String loanRequestJson = jsonTester.write(loanRequest).getJson();
    given(this.loanServiceMock.applyForLoan(any(LoanRequest.class), anyString())).willThrow(LoanRequestLimitExceeded.class);

    this.mockMvc.perform(post("/loans/apply").contentType(MediaType.APPLICATION_JSON)
        .content(loanRequestJson))
        .andExpect(status().is(HttpStatus.TOO_MANY_REQUESTS.value()));
  }

  @Test
  public void apply_blacklisted_user() throws Exception {
    String loanRequestJson = jsonTester.write(loanRequest).getJson();
    given(this.loanServiceMock.applyForLoan(any(LoanRequest.class), anyString())).willThrow(BlacklistUser.class);

    this.mockMvc.perform(post("/loans/apply").contentType(MediaType.APPLICATION_JSON)
        .content(loanRequestJson))
        .andExpect(status().is(HttpStatus.FORBIDDEN.value()));
  }


  private List<Loan> createdLoanList() {
    List<Loan> loans = new ArrayList<>();
    loans.add(new Loan());
    loans.add(new Loan());
    return loans;
  }
}