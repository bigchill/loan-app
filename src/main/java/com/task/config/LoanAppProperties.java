package com.task.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LoanAppProperties {

  @Value("${ip-resolver.service-url}")
  private String ipServiceUrl;

  @Value("${ip-resolver.response-format}")
  private String ipServiceDataFormat;

  @Value("${ip-resolver.default-country-code}")
  private String defaultCountryCode;

  @Value("${ip-resolver.default-country-name}")
  private String defaultCountryName;

  @Value("${loan-application.request-count-limit}")
  private Integer loanRequestCountLimit;

  @Value("${loan-application.request-interval-limit}")
  private Integer loanRequestIntervalLimitInSeconds;

  public LoanAppProperties() {
  }

  public String getIpServiceUrl() {
    return ipServiceUrl;
  }

  public String getIpServiceDataFormat() {
    return ipServiceDataFormat;
  }

  public String getDefaultCountryCode() {
    return defaultCountryCode;
  }

  public String getDefaultCountryName() {
    return defaultCountryName;
  }

  public Integer getLoanRequestCountLimit() {
    return loanRequestCountLimit;
  }

  public Integer getLoanRequestIntervalLimitInSeconds() {
    return loanRequestIntervalLimitInSeconds;
  }
}