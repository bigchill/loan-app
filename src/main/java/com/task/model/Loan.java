package com.task.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
@Entity
@Table(name = "loans")
public class Loan {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private Date created;

  private BigDecimal amount;

  private String currency;

  private Integer term;

  private BigDecimal interestRate;

  private Long userId;

  private String countryCode;

  @PrePersist
  protected void onCreate() {
    created = new Date();
  }
}
