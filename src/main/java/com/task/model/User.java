package com.task.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

import lombok.Data;

@Data
@Entity
@Table(name = "users")
public class User {

  @Id
  @GeneratedValue
  private Long id;

  private String firstname;

  private String lastname;

  private String personalId;

  @OneToMany
  @JoinColumn(name = "userId", referencedColumnName = "id")
  private List<Loan> loans;
}
