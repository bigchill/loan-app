package com.task.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(exclude = {"id"})
@EqualsAndHashCode
@Entity
@Table(name = "blacklist")
public class Blacklist {

  @Id
  @GeneratedValue
  private Long id;

  private String personalId;

  private String reason;
}
