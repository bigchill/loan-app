package com.task.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.task.repository.BlacklistRepository;

@Service
public class BlacklistService {

  private static final Logger log = LoggerFactory.getLogger(BlacklistService.class);
  private BlacklistRepository blacklistRepository;

  public BlacklistService(BlacklistRepository blacklistRepository) {
    this.blacklistRepository = blacklistRepository;
  }

  public boolean isBlacklistUser(String userPersonalId) {
    log.debug("Searching user {} in blacklist", userPersonalId);
    return blacklistRepository.findByPersonalId(userPersonalId).isPresent();
  }
}
