package com.task.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.task.config.LoanAppProperties;
import com.task.controller.api.LoanRequest;
import com.task.error.BlacklistUser;
import com.task.error.LoanNotFound;
import com.task.error.LoanRequestLimitExceeded;
import com.task.intergration.dto.CountryResponse;
import com.task.intergration.exception.CountryNotFound;
import com.task.intergration.exception.IpResolverServiceUnavailable;
import com.task.intergration.service.IpResolverService;
import com.task.model.Loan;
import com.task.repository.LoanRepository;

@Service
public class LoanService {

  private static final Logger log = LoggerFactory.getLogger(LoanService.class);
  private LoanRepository loanRepository;
  private BlacklistService blacklistService;
  private IpResolverService ipResolverService;
  private LoanAppProperties loanAppProperties;

  public LoanService(LoanRepository loanRepository, BlacklistService blacklistService, IpResolverService ipResolverService, LoanAppProperties loanAppProperties) {
    this.loanRepository = loanRepository;
    this.blacklistService = blacklistService;
    this.ipResolverService = ipResolverService;
    this.loanAppProperties = loanAppProperties;
  }

  public List<Loan> findAllLoans() {
    return loanRepository.findAll();
  }

  public List<Loan> findUserLoans(Long userId) {
    List<Loan> loans = loanRepository.findByUserId(userId);
    if (loans == null || loans.isEmpty()) {
      log.debug("Not found loans for user with id " + userId);
      throw new LoanNotFound("Not found loans for user");
    }
    return loans;
  }


  public Loan applyForLoan(LoanRequest loan, String remoteAddress) {
    validateUserForLoyalty(loan.personalId);
    CountryResponse countryResponse = populateCountryCode(remoteAddress);

    if (hasExceededRequestLimit(countryResponse.getCountryCode())) {
      log.debug("Loans applications count for " + countryResponse.getCountryName() + " has been exceeded");
      throw new LoanRequestLimitExceeded("Loans application count has been exceeded");
    }
    Loan loanModel = new Loan();
    loanModel.setAmount(loan.amount);
    loanModel.setCurrency(loan.currency);
    loanModel.setTerm(loan.term);
    loanModel.setInterestRate(loan.interestRate);
    loanModel.setUserId(loan.userId);
    loanModel.setCountryCode(countryResponse.getCountryCode());
    return loanRepository.save(loanModel);
  }

  private void validateUserForLoyalty(String personalId) {
    if (blacklistService.isBlacklistUser(personalId)) {
      log.debug("User - {} is from blacklist ", personalId);
      throw new BlacklistUser("User " + personalId + " is from blacklist");
    }
  }

  private CountryResponse populateCountryCode(String remoteAddress) {
    CountryResponse countryResponse;
    try {
      countryResponse = ipResolverService.resolveCountry(remoteAddress);
    }
    catch (CountryNotFound | IpResolverServiceUnavailable e) {
      log.debug("Something wrong with remote rest service. Applying default country");
      countryResponse = new CountryResponse(loanAppProperties.getDefaultCountryCode(), loanAppProperties.getDefaultCountryName());
    }
    return countryResponse;
  }

  private boolean hasExceededRequestLimit(String countryCode) {
    Integer requestsLimit = loanAppProperties.getLoanRequestCountLimit();
    Integer intervalLimit = loanAppProperties.getLoanRequestIntervalLimitInSeconds();

    Date startDate = new Date(System.currentTimeMillis() - intervalLimit * 1000);
    Date endDate = new Date();
    List<Loan> loans = loanRepository.findByCreatedBetweenAndCountryCode(startDate, endDate, countryCode);

    return loans.size() >= requestsLimit;
  }
}
