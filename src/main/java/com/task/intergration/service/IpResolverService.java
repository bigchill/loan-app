package com.task.intergration.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.task.config.LoanAppProperties;
import com.task.intergration.dto.CountryResponse;
import com.task.intergration.exception.CountryNotFound;
import com.task.intergration.exception.IpResolverServiceUnavailable;

@Component
public class IpResolverService {

  private static final Logger log = LoggerFactory.getLogger(IpResolverService.class);

  private RestTemplate restTemplate;
  private LoanAppProperties properties;

  public IpResolverService(RestTemplate restTemplate, LoanAppProperties properties) {
    this.restTemplate = restTemplate;
    this.properties = properties;
  }


  public CountryResponse resolveCountry(String ipAddress) {
    log.debug("Calling remote service: " + buildRestServiceUrl() + ipAddress);

    try {
      ResponseEntity<CountryResponse> countryResponse = restTemplate.getForEntity(buildRestServiceUrl() + ipAddress, CountryResponse.class);
      if (countryResponse.getStatusCode().is2xxSuccessful()) {
        log.debug("IP address " + ipAddress + " is successfully resolved for " + countryResponse.getBody().getCountryName());
        return countryResponse.getBody();
      }
    }
    catch (HttpClientErrorException e4xx) {
      // Exception thrown when an HTTP 4xx is received
      if (HttpStatus.NOT_FOUND.equals(e4xx.getStatusCode())) {
        log.warn("Can not resolve country by IP address: " + ipAddress);
        throw new CountryNotFound("Not found country for IP address: " + ipAddress);
      }
    }
    catch (HttpServerErrorException e5xx) {
      // Exception thrown when an HTTP 5xx is received
      log.warn("IP Resolver service is currently unavailable");
      throw new IpResolverServiceUnavailable("IP Resolver service is currently unavailable");
    }
    catch (RestClientException e) {
      log.warn("Something wrong with IP Resolver service");
    }
    throw new IpResolverServiceUnavailable("Something wrong with IP Resolver service");
  }

  private String buildRestServiceUrl() {
    return properties.getIpServiceUrl() + "/" + properties.getIpServiceDataFormat() + "/";
  }
}
