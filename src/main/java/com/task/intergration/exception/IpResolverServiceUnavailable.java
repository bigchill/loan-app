package com.task.intergration.exception;

public class IpResolverServiceUnavailable extends RuntimeException {

  public IpResolverServiceUnavailable(String message) {
    super(message);
  }
}
