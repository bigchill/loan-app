package com.task.controller.api;

import java.math.BigDecimal;

public class LoanRequest {

  public BigDecimal amount;
  public String currency;
  public Integer term;
  public BigDecimal interestRate;
  public String personalId;
  public Long userId;

  public LoanRequest() {
  }

}
