package com.task.controller.api;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class LoanResponse {

  public BigDecimal amount;
  public String currency;
  public Integer term;
  public BigDecimal interestRate;
  public Long userId;
  public String countryCode;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
  public Date created;

  public LoanResponse() {
  }
}
