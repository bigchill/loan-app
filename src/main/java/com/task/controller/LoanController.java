package com.task.controller;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task.controller.api.LoanRequest;
import com.task.controller.api.LoanResponse;
import com.task.model.Loan;
import com.task.service.LoanService;
import com.task.util.Mapper;

@RestController
@RequestMapping("/loans")
public class LoanController {

  private static final Logger log = LoggerFactory.getLogger(LoanController.class);
  private LoanService service;

  public LoanController(LoanService service) {
    this.service = service;
  }


  @PostMapping("/apply")
  public LoanResponse apply(@RequestBody LoanRequest loanRequest, HttpServletRequest request) {
    String remoteAddress = request.getRemoteAddr();
    Loan loan = service.applyForLoan(loanRequest, remoteAddress);
    return new Mapper().map2LoanResponse(loan);
  }

  @GetMapping("/")
  public List<LoanResponse> list() {
    List<Loan> loans = service.findAllLoans();
    return new Mapper().map2LoanResponse(loans);
  }

  @GetMapping("/{userId}")
  public List<LoanResponse> getByUserId(@PathVariable(value = "userId") Long userId) {
    log.debug("Retrieving all loans for user with id " + userId);
    List<Loan> loans = service.findUserLoans(userId);
    return new Mapper().map2LoanResponse(loans);
  }
}
