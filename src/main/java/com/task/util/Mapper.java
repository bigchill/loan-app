package com.task.util;

import java.util.ArrayList;
import java.util.List;

import com.task.controller.api.LoanResponse;
import com.task.model.Loan;

public class Mapper {

  public List<LoanResponse> map2LoanResponse(List<Loan> loans) {
    List<LoanResponse> loansApi = new ArrayList<>();
    for (Loan loan : loans) {
      loansApi.add(map2LoanResponse(loan));
    }
    return loansApi;
  }

  public LoanResponse map2LoanResponse(Loan loan) {
    LoanResponse loanApi = new LoanResponse();
    loanApi.amount = loan.getAmount();
    loanApi.currency = loan.getCurrency();
    loanApi.interestRate = loan.getInterestRate();
    loanApi.term = loan.getTerm();
    loanApi.userId = loan.getUserId();
    loanApi.created = loan.getCreated();
    loanApi.countryCode = loan.getCountryCode();

    return loanApi;
  }
}
