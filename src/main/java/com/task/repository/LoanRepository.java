package com.task.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.task.model.Loan;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Long> {

  List<Loan> findByUserId(Long userId);

  List<Loan> findByCreatedBetweenAndCountryCode(Date startDate, Date endDate, String countryCode);
}
