package com.task.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.task.model.Blacklist;

@Repository
public interface BlacklistRepository extends JpaRepository<Blacklist, Long> {

  Optional<Blacklist> findByPersonalId(String personalId);
}
