package com.task.error;

public class LoanNotFound extends RuntimeException {

  public LoanNotFound(String message) {
    super(message);
  }
}
