package com.task.error;

public class BlacklistUser extends RuntimeException {

  public BlacklistUser() {
  }

  public BlacklistUser(String message) {
    super(message);
  }
}
