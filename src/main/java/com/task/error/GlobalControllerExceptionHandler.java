package com.task.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.task.controller.api.ErrorResponse;

@RestControllerAdvice
public class GlobalControllerExceptionHandler {

  private static final Logger logger = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

  @ExceptionHandler(value = LoanNotFound.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ErrorResponse processLoanNotFound(LoanNotFound ex) {
    logger.debug("Could not find Loans for user {}", ex.getMessage(), ex);
    return new ErrorResponse(String.valueOf(HttpStatus.NOT_FOUND.value()), ex.getMessage());
  }

  @ExceptionHandler(value = BlacklistUser.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  public ErrorResponse processBlacklistUser(BlacklistUser ex) {
    logger.debug("User is from black list {}", ex.getMessage(), ex);
    return new ErrorResponse(String.valueOf(HttpStatus.FORBIDDEN.value()), ex.getMessage());
  }

  @ExceptionHandler(value = LoanRequestLimitExceeded.class)
  @ResponseStatus(HttpStatus.TOO_MANY_REQUESTS)
  public ErrorResponse processLoanRequestLimitExceeded(LoanRequestLimitExceeded ex) {
    logger.debug("Limit of requests per country exceeded", ex.getMessage(), ex);
    return new ErrorResponse(String.valueOf(HttpStatus.TOO_MANY_REQUESTS.value()), ex.getMessage());
  }
}
