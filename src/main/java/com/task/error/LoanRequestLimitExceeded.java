package com.task.error;

public class LoanRequestLimitExceeded extends RuntimeException {

  public LoanRequestLimitExceeded(String message) {
    super(message);
  }
}
