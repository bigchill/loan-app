-- Users
INSERT INTO users (id, firstname, lastname, personal_id) VALUES (1, 'Willie', 'Sutton', 'A45366D2-B4F7');
INSERT INTO users (id, firstname, lastname, personal_id) VALUES (2, 'John', 'Dillinger', '3C65407D-DDCB');
INSERT INTO users (id, firstname, lastname, personal_id) VALUES (3, 'Jesse', 'James', '91804345-1904');
INSERT INTO users (id, firstname, lastname, personal_id) VALUES (4, 'Butch', 'Cassidy', '7596F068-8609');


-- Loans
INSERT INTO loans (id, created, amount, currency, term, interest_rate, user_id, country_code)
  VALUES (1, '2017-04-03 22:02:00.175', 7500.0, 'EUR', 90, 20.5, 1, 'LV');
INSERT INTO loans (id, created, amount, currency, term, interest_rate, user_id, country_code)
  VALUES (2, '2017-04-04 06:02:00.243', 2500.0, 'EUR', 180, 18, 1, 'LV');

INSERT INTO loans (id, created, amount, currency, term, interest_rate, user_id, country_code)
  VALUES (3, NOW(), 1500.0, 'EUR', 60, 18, 3, 'LV');


-- Blacklist
INSERT INTO blacklist (id, personal_id, reason) VALUES (1, '3C65407D-DDCB', 'Unreliable borrower');
