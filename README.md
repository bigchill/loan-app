#Loan application for TWINO 

Loan application exposes REST API endpoints:
  * list all loans 
    * ```GET /loans/```
  * list user's loan 
    * ```GET /loans/{iserId}```
  * apply for loan
    * ```POST /loans/apply```
    * Example of JSON sent for loan application:
```
    {
      "amount" : 3000.0,
      "currency" : "EUR",
      "term" : 180,
      "interestRate" : 18.5,
      "personalId" : "A45366D2-B4F7",
      "userId" : 1
    }
```


#### Creating and running an executable jar

To create an executable jar you need to run ```mvn package``` from the command line:

```$ mvn package```

To run this application use the ```java -jar``` command:

```$ java -jar target/loan-app-0.0.1-SNAPSHOT.jar```

